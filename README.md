<style> @import url("documentation/mdstyle.css"); </style>

# REACT QUICKSTART #

This Repository provides a starting point for creating a React Front-end using Typescript and webpack. 
It uses the .net core web app opinions as to where in the folder structure thigns should go, and uses Redux for state management.

### What is this repository for? ###

* Basic functionality created, node modules, webpack setup etc.
* [Markdown reference](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
	* Ensure that nodejs (*v6 + *) and thereby npm is installed and updated on your system, and included in the global path.
* Configuration
	* In Visual Studio, we need to add the path to the installed (updated) node version.<br />
	<details>
		<summary>Click for instructions with images</summary>
[Tutorial Page](http://www.ryadel.com/en/visual-studio-2015-update-nodejs-andor-npm-latest-version/)
	 	Basically : 
		* install nodejs.
		* update npm from the install location
		* Add the path above the current path in visual studio's options
	![](documentation/visual-studio-2015-nodejs-options-2.jpg)
	</details> 
	* From Command Line
		* **npm install**
                * **npm install typings -g**
                * **npm install webpack -g**
		* **typings i**
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions
	* From visual studio, run the "Watch - Development" task to generate JS files
	* run the "Serve Hot" task, and open the [local dev server](http://localhost:8080/wwwroot/)
* Useful plugins

### Plugins for Visual Studio ###
* [Webpack task runner](https://marketplace.visualstudio.com/items?itemName=MadsKristensen.WebPackTaskRunner)
* [Typescript 2.3.1 - Support 2015](https://www.microsoft.com/en-us/download/details.aspx?id=48593)

### Plugins for Chrome ###
* [React Developer Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en)
* [Redux Developer Tools](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=en)