/// <binding ProjectOpened='Watch - Development' />
// requires
var webpack = require("webpack");
var path = require("path");
var CompressionPlugin = require("compression-webpack-plugin");
//var zopfli = require('node-zopfli');
var HtmlWebpackPlugin = require('html-webpack-plugin');


// plugin configuration
var watchIgnore = new webpack.WatchIgnorePlugin([/(node_modules)/]);
var uglify = new webpack.optimize.UglifyJsPlugin({ compress: { warnings: false, drop_debugger: false } });
var zopfiCompress = new CompressionPlugin({
    asset: "[path].gz[query]",
    algorithm: "zopfli",
    test: /\.js$|\.html$/,
    threshold: 1024,
    minRatio: 0.8,
    numiterations: 3 // 100 better, but who has time!?
});

__dirname = __dirname.charAt(0).toUpperCase() + __dirname.slice(1);

// webpack config
module.exports = { //function(env) {
    entry: {
        "entry": "./TS/entry",
        "bundle": "./TS/bundle",
        //,react: ["react", "react-dom", "react-bootstrap", "react-bootstrap-table", "react-chartjs-2"],
        //externals: ["moment", "jquery", "bootstrap", "redux", "lodash"],
        //common: ["./TS/Constants", "./TS/Common/common", "./TS/Common/Login", "./TS/State/State", "./TS/State/LoginState", "./TS/Common/Tracker"]
    },
    output: {
        path: path.join(__dirname, './wwwroot/Script/dist/'),
        publicPath: '/Script/dist/',
        filename: "[name].min.js"
    },
    // devtool Breaks the production watcher..
    devtool: "source-map-loader",     // Enable sourcemaps for debugging webpack's output.
    //devtool: "hidden-source-map",     // Enable sourcemaps for debugging webpack's output.
    //devtool: 'inline-source-map',
    resolve: {
        extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
    },

    module: {
        // preloaders have changed in webpack 2.1....
        //http://stackoverflow.com/questions/39919793/tslint-loader-with-webpack-2-1-0-beta-25
        rules: [
            {
                enforce: 'pre',
                test: /\.[jt]sx$/,
                loader: 'source-map-loader',
                exclude: /(node_modules)/
            },
            {
                test: /\.tsx?$/, //  /.+(tsx|html)?$/, //
                // loaders execute right to left..
                loaders: [
                    {
                        loader: "babel-loader",
                        query: {
                            presets: [['es2015']] //, {"modules": false}
                        }
                    },
                    {
                        loader: "awesome-typescript-loader" /* ts-loader */
                        /**
                            if breaks with typescript 2.1.4,
                            replace the awesome-typescript-loader function as documented here:
                            https://github.com/s-panferov/awesome-typescript-loader/issues/293
                        */
                    }
                ],
                exclude: [
                    path.resolve(__dirname, "node_modules")
                ]
            }
        ]
    },

    plugins: [
        //uglify,
        //new webpack.optimize.CommonsChunkPlugin({
        //    //minChunks: Infinity,
        //    names: ['common', 'react', 'externals', 'manifest'] // Specify the common bundle's name.
        //}),
        //zopfiCompress,
        watchIgnore,
        function () {
            this.plugin("done", function (stats) {
                if (stats.compilation.errors &&
                    stats.compilation.errors.length &&
                    process.argv.indexOf('--watch') === -1) {
                    console.log(stats.compilation.errors);
                    process.exit(1); // or throw new Error('webpack build failed.');
                }
                // ...
            });
        }
        //,
        //new webpack.SourceMapDevToolPlugin({
        //    columns: false,
        //    filename: '[file].map[query]',
        //    lineToLine: false,
        //    module: false,
        //    append: '\n//# sourceMappingURL=/_karma_webpack_/[url]'
        //})
    ],
    // When importing a module whose path matches one of the following, just
    // assume a corresponding global variable exists and use that instead.
    // This is important because it allows us to avoid bundling all of our
    // dependencies, which allows browsers to cache those libraries between builds.
    externals: {
        "jquery": "jQuery",
        "react": "React",
        "react-dom": "ReactDOM",
        "react-bootstrap": "ReactBootstrap",
        "redux": "Redux",
        "react-redux": "ReactRedux",
        "react-select": "Select"
    }
};
