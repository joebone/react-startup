import * as React from "react";
import * as ReactDOM from "react-dom";

//class App extends React 

interface IAppProps { }
interface IAppComponentState { }

//export class App extends React.Component<IAppProps, IAppComponentState> {
export default class App extends React.Component<IAppProps, {}> {

    range(start, end, step, offset) {

        var len = (Math.abs(end - start) + ((offset || 0) * 2)) / (step || 1) + 1;
        var direction = start < end ? 1 : -1;
        var startingPoint = start - (direction * (offset || 0));
        var stepSize = direction * (step || 1);

        return Array(len).fill(0).map(function (_, index) { return startingPoint + (stepSize * index); });

    }

    render() {
        console.log('App render method called.');

        var returnComponent =
            <div>
                Hello world from React too as well...
                <p>We can just put html in here</p>
                <p>But remember, it isn't actually html...</p>
                <pre>{this.range(0, 20, 1, 0).join(",")}</pre>
            </div>;


        return returnComponent;
    }
}

console.log("entry.tsx has been called.");
ReactDOM.render(<App />, document.getElementById('app'));